/*
 * StudentEntity.java
 * Copyright(C) 贵州龙马融合信息技术股份有限责任公司
 * All rights reserved.
 * -------------------------------
 * 2018-07-19 13:59:10生成
 */
package com.wujie.spring.springbootmybatis.pojo;

/**
 * 数据库表student对应的实体对象
 * @author wujie 
 * @version 1.0 
 * 2018-07-19 13:59:10
 */
public class StudentEntity {
    private Integer id;

    private String name;

    private Integer age;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}