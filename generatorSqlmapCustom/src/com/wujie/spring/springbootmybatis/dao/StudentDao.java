/*
 * StudentDao.java
 * Copyright(C) 贵州龙马融合信息技术股份有限责任公司
 * All rights reserved.
 * -------------------------------
 * 2018-07-19 13:59:10生成
 */
package com.wujie.spring.springbootmybatis.dao;

import com.wujie.spring.springbootmybatis.pojo.StudentEntity;

public interface StudentDao {
    int deleteByPrimaryKey(Integer id);

    int insert(StudentEntity record);

    int insertSelective(StudentEntity record);

    StudentEntity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(StudentEntity record);

    int updateByPrimaryKey(StudentEntity record);
}